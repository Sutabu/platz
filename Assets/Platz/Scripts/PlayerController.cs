﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Personagem
{
    public static PlayerController instance;

    [Header("SFX Player")]

    [Tooltip("Som de tiro do player")]
    [SerializeField] private AudioClip somTiro;

    [Tooltip("Som de Quando Morre")]
    public AudioClip somMorte;


    [Header("Controle do Personagem")]

    [Tooltip("Determina a velocidade de movimento do jogador")]
    public float velMove;
    
    //Variáveis que recebem 
    private float moveX;
    private float moveY;

    [Tooltip("Responsável pela física do personagem")]
    public Rigidbody2D rb2d;

    [Tooltip("Responsável pela colisão")]
    [SerializeField] private CapsuleCollider2D colliderNave;


    //Animnação do Personagem
    [Tooltip("Responsável pelas animações do personagem")]
    [SerializeField] private Animator anim;


    [Header("Sistema de Ataque")]

    [Tooltip("Manuseia o objeto 'Míssil' nível 1")]
    public GameObject missil1;

    [Tooltip("Manuseia o objeto 'Míssil' nível 2")]
    public GameObject missil2;

    [Tooltip("Define a posição do míssil")]
    public Transform posMissil;

    [Tooltip("Define a posição 1 do míssil melhorado")]
    public Transform posMissilUp1;

    [Tooltip("Define a posição 2 do míssil melhorado")]
    public Transform posMissilUp2;

    [Tooltip("Diz quando o último míssil foi lançado")]
    private float tempoMissil;

    [Tooltip("Define o tempo entre cada míssil lançado")]
    public float tempoEntreMissil;


    [Header("Sistema de Upgrade")]

    [Tooltip("Manuseia o nível atual do míssil")]
    public int levelAtual;
    [Tooltip("Manuseia o nível máximo do míssil")]
    public int levelMax;


    //Verifica se pode pausar
    private bool podePausar;

    private void Update()
    {
        AnimaPlayer();

        //Verifica se houve comando do jogador
        moveX = Input.GetAxisRaw("Horizontal") * velMove;
        moveY = Input.GetAxisRaw("Vertical") * velMove;

        FlipHorizontal();
        Atirar();
    }

    private void FixedUpdate()
    {
        //Executa o comando de movimento
        Movimento();
    }

    private void Start()
    {
        instance = this; //Aplica vinformação à instancia
        podePausar = true; //No início do jogo, é possível jogar
    }

    private void Movimento()
    {
        //Se o jogador estiver morto, não poderá movimentar o personagem
        if (morto) return;

        //Aplica a velocidade de movimento no jogador
        Vector2 movimento = new Vector2(moveX, moveY);
        rb2d.velocity = movimento;
    }

    private void AnimaPlayer()
    {
        bool movendo = moveX != 0;
        bool moveTras = moveY < 0;

        anim.SetBool("Morto", morto);
        anim.SetBool("MovendoX", movendo);
        anim.SetBool("MoveTras", moveTras);
        anim.SetBool("Escudo", vidaAtual > 1);   
    }

    private void FlipHorizontal()
    {
        Vector2 scale = transform.localScale;

        if(moveX < 0)
        {
            scale.x = -0.8f;
            transform.localScale = scale;

            colliderNave.size = new Vector2(1, colliderNave.size.y);
        }
        else if(moveX > 0)
        {
            scale.x = 0.8f;
            transform.localScale = scale;
            colliderNave.size = new Vector2(1, colliderNave.size.y);
        }
        else
            colliderNave.size = new Vector2(1.28f, colliderNave.size.y);
    }

    public void Atirar()
    {
        if(Input.GetKey(KeyCode.C) || Input.GetKey(KeyCode.J))
        {
            if(morto) return;

            if(Time.time > tempoMissil + tempoEntreMissil)
            {
                SFX.instance.PlaySom(somTiro);

                //Atualiza o tempo que se passou entre cada tiro
                tempoMissil = Time.time;

                //Checa o nível do jogador e aplica o poder novo
                switch (levelAtual)
                {
                    case 0:
                        Instantiate(missil1, posMissil.position, posMissil.rotation);
                        break;

                    case 1:
                        Instantiate(missil1, posMissilUp1.position, posMissilUp1.rotation);
                        Instantiate(missil1, posMissilUp2.position, posMissilUp2.rotation);
                        break;

                    case 2:
                        Instantiate(missil2, posMissil.position, posMissil.rotation);
                        break;

                    case 3:
                        Instantiate(missil2, posMissilUp1.position, posMissilUp1.rotation);
                        Instantiate(missil2, posMissilUp2.position, posMissilUp2.rotation);
                        break;

                    default:
                        break;
                }
            }
        }   
    }

    public void TiroLevel(int up)
    {
        levelAtual = Mathf.Min(levelAtual + up, levelMax);
    }

    public override void Morte()
    {
        SFX.instance.PlaySom(somMorte);

        Debug.Log("Game Over");
    }
}
