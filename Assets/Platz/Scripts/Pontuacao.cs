﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pontuacao : MonoBehaviour
{
    public static Pontuacao instance;
    public int pontuacao;
    public int highScore;

    private void Start()
    {
        instance = this;
        highScore = PlayerPrefs.GetInt("highScore", 0);
    }

    public void Ponto(int score)
    {
        pontuacao += score;

        if (pontuacao > highScore)
        {
            highScore = pontuacao;
            PlayerPrefs.SetInt("highScore", highScore);
        }   
    }
}
