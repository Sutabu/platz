﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTiro : MonoBehaviour
{
    [Tooltip("Responsável por aplicar a velocidade do míssil")]
    [SerializeField] private Rigidbody2D rb2d;

    [Tooltip("Define a velocidade do míssil")]
    public float velMissil;
    [Tooltip("Define o dano do míssil")]
    public float danoMissil;
    [Tooltip("Define o tempo que o míssil fica na tela")]
    public float vidaMissil;
    

    void Start()
    {
        rb2d.velocity = new Vector2(0, velMissil);
        Destroy(gameObject, vidaMissil);
    }

    private void OnTriggerEnter2D(Collider2D outro)
    {
        if(outro.CompareTag("Inimigo"))
        {
            outro.GetComponent<Meteoro>().RecebeDano(danoMissil);
            Destroy(gameObject);
        }
    }
}
