﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteoro : Personagem
{
    //dano que o meteoro causa
    //private float danoMeteoro = 1;

    [Header("Movimentação")]

    [Tooltip("Define o tempo que o meteoro fica na tela")]
    [SerializeField] private Rigidbody2D rb2d;

    Vector2 movimento;

    [Tooltip("Define a velocidade em X do meteoro")]
    [SerializeField] private float  velMeteoroX;

    [Tooltip("Define a velocidade em Y do meteoro")]
    [SerializeField] private float velMeteoroY;

    [SerializeField] private float minX, maxX;

    [Tooltip("Define o tempo que o meteoro fica na tela")]
    public float tempoMeteoro;


    [Header("Ajustes")]

    [SerializeField] private Animator anim;

    [Tooltip("Define a quantidade de pontos que o jogador recebe ao ser destruído")]
    public int pontos;

    [Tooltip("Manuseia o componente 'Colisão'")]
    [SerializeField] private Collider2D colisao;

    [Tooltip("Manuseia os componentes dos itens a serem dropados")]
    [SerializeField] private GameObject itemDrop;

    public Transform posMeteoro;

    [Header("Item Drop")]
    [Tooltip("Número em que se baseia a chance de dropar um item")]
    public int chanceDrop;

    //Aumento de velocidade dos meteoros com a pontuação do jogador:
    //Número em que se baseia o aumento de velocidade de acordo com a pontuação do jogador
    private int checaVelMet;

    //Número que aumenta a velocidade do meteoro
    private int aumentaVelMet;


    private void OnTriggerEnter2D(Collider2D outro)
    {
        if(outro.CompareTag("Player"))
        {
            if(!outro.GetComponent<PlayerController>().morto)
            {
                outro.GetComponent<PlayerController>().RecebeDano(1);
                RecebeDano(1);
            }
        }
    }

    private void Start()
    {
        checaVelMet = 150; //Pontuação inicial a ser atingida para que a velocidade aumente
        aumentaVelMet = 0; //Valor inicial da variável que aumenta a velocidade 

        Destroy(gameObject, tempoMeteoro);
    }

    private void Update()
    {
        AniMeteoro(); //Aplica animação do meteoro

        movimento = new Vector2(velMeteoroX, -velMeteoroY + -aumentaVelMet);
        rb2d.velocity = movimento;

        AumentaVelMet(); //Aplica o aumento de velocidade
    }

    public void AniMeteoro()
    {
        morto = vidaAtual <= 0;
        anim.SetBool("Morto", morto);
    }

    public override void Morte()
    {
        Destroy(colisao);
        Pontuacao.instance.Ponto(pontos);

        DropItem(); //Chance de criar um item ao ser destruído

        Debug.Log("Meteoro morto");
    }

    public void DropItem()
    {
        if (PlayerController.instance.vidaAtual < 2 && transform.localScale.x < 0.5)
        {
            int calcDrop = Random.Range(1, 10);

            if (calcDrop > chanceDrop)
            {
                Instantiate(itemDrop, posMeteoro.position, posMeteoro.rotation);
            }
        }
        else if (PlayerController.instance.levelAtual < PlayerController.instance.levelMax && transform.localScale.x > 0.5)
        {
            int calcDrop = Random.Range(1, 10);

            if (calcDrop > chanceDrop)
            {
                Instantiate(itemDrop, posMeteoro.position, posMeteoro.rotation);
            }
        }
    }

    public void AumentaVelMet()
    {
        if (Pontuacao.instance.pontuacao > checaVelMet)
        {
            checaVelMet *= 2;
            aumentaVelMet++;
        }
    }
}
