﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameController : MonoBehaviour
{
    [SerializeField] private float limiteMinX;

    [SerializeField] private float limiteMaxX;

    [SerializeField] private float alturaY;

    [SerializeField] private float intervalo;

    [SerializeField] private GameObject prefabMeteoroPequeno;

    [SerializeField] private GameObject prefabMeteoroGrande;

    public bool podePausar;

    public GameObject telaControles;
    public GameObject pauseMenu;
    public GameObject telaGameOver;

    [SerializeField] private TextMeshProUGUI highScoreTXT;
    [SerializeField] private TextMeshProUGUI PlacarTXT;

    private void Start()
    {
        InvokeRepeating("CriaMeteoroPequeno", intervalo, intervalo);
        InvokeRepeating("CriaMeteoroGrande", intervalo + 5, intervalo + 5);

        highScoreTXT.text = "HighScore: " + PlayerPrefs.GetInt("highScore", 0).ToString();

        podePausar = true;
    }

    private void Update()
    {
        //Pausa o jogo
        if (Input.GetKeyDown(KeyCode.Escape) && !pauseMenu.activeInHierarchy)
        {
            Pause();
        }
        //Retorna ao jogo
        else if (Input.GetKeyDown(KeyCode.Escape) && pauseMenu.activeInHierarchy)
        {
            Resume();
        }
   
        PlacarTXT.text = Pontuacao.instance.pontuacao.ToString();

        GameOver();
    }

    private void CriaMeteoroPequeno()
    {
        float x = Random.Range(limiteMinX, limiteMaxX);
        Vector2 pos = new Vector2(x, alturaY);

        Instantiate(prefabMeteoroPequeno, pos, transform.rotation);
    }

    private void CriaMeteoroGrande()
    {
        float x = Random.Range(limiteMinX, limiteMaxX);
        Vector2 pos = new Vector2(x, alturaY);

        Instantiate(prefabMeteoroGrande, pos, transform.rotation);
    }

    public void Pause()
    {
        if(!PlayerController.instance.morto && podePausar)
        {
            podePausar = false;
            Time.timeScale = 0;
            pauseMenu.SetActive(true);
        }
    }

    public void Resume()
    {
        podePausar = true;
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
    }

    public void TelaControles()
    {
        pauseMenu.SetActive(false);
        telaControles.SetActive(true);

    }

    public void Voltar()
    {
        telaControles.SetActive(false);
        pauseMenu.SetActive(true);
    }

    public void Home()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Menu");
    }

    public void Retry()
    {
        SceneManager.LoadScene("Training");
    }

    public void GameOver()
    {
        if(PlayerController.instance.morto)
        {
            telaGameOver.SetActive(true);
        }
    }
}
