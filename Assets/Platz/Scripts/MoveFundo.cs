﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFundo : MonoBehaviour
{
    [Header("Movimentação do Fundo")]

    [Tooltip("Velocidade a qual o fundo se move")]
    public float velMoveFundo;
    [Tooltip("Responsável por aplicar a movimentação no fundo")]
    [SerializeField] private Rigidbody2D posFundo1;
    [SerializeField] private Rigidbody2D posFundo2;
    [SerializeField] private Rigidbody2D posFundo3;

    [Header("Reset da posição")]

    [Tooltip("Armazena o tamanho do fundo (Y)")]
    public float tamanhoFundo;
    [Tooltip("Armazena quantos fundos existem na cena")]
    public float quantFundo;
   

    private void OnTriggerEnter2D(Collider2D outro)
    {
        if(outro.CompareTag("Fundo"))
        {
            Vector3 resetPos = outro.transform.position;
            resetPos.y += tamanhoFundo * quantFundo;
            outro.transform.position = resetPos;
        }
    }

    private void Start()
    {
        MovimentaFundo();
    }

    public void MovimentaFundo()
    {
        Vector2 posFundo = new Vector2(0, -1);
        posFundo *= velMoveFundo;
        posFundo1.velocity = posFundo;
        posFundo2.velocity = posFundo;
        posFundo3.velocity = posFundo;

    }
}
