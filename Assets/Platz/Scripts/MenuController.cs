using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuController : MonoBehaviour
{
    public static MenuController instance;

    public GameObject MenuPrincipal;
    public GameObject MenuAjuda;
    public GameObject MenuControles;
    public GameObject MenuObjetivos;

    public TextMeshProUGUI highScoreTXT;

    private void Start()
    {
        instance = this;
        highScoreTXT.text = "HighScore:" + PlayerPrefs.GetInt("highScore", 0).ToString();

        Btn_MenuPrincipal();
    }

    public void Btn_Jogar()
    {
        SceneManager.LoadScene("Training");
    }

    public void Btn_Ajuda()
    {
        MenuPrincipal.SetActive(false);
        MenuAjuda.SetActive(true);
        MenuControles.SetActive(false);
        MenuObjetivos.SetActive(false);
    }

    public void Btn_MenuPrincipal()
    {
        MenuPrincipal.SetActive(true);
        MenuAjuda.SetActive(false);
        MenuControles.SetActive(false);
        MenuObjetivos.SetActive(false);
    }

    public void Btn_MenuControles()
    {
        MenuPrincipal.SetActive(false);
        MenuAjuda.SetActive(false);
        MenuControles.SetActive(true);
        MenuObjetivos.SetActive(false);
    }

    public void Btn_MenuObjetivos()
    {
        MenuPrincipal.SetActive(false);
        MenuAjuda.SetActive(false);
        MenuControles.SetActive(false);
        MenuObjetivos.SetActive(true);
    }

    public void Btn_Sair()
    {
        Application.Quit();
    }
}
