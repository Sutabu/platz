﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coletaveis : MonoBehaviour
{
    public enum TipoItem //Tipo de itens coletáveis
    {
        escudo,
        lvUp
    }

    [Tooltip("Som de coletar item")]
    [SerializeField] private AudioClip somEscudo;

    [Tooltip("Som de coletar item")]
    [SerializeField] private AudioClip somLevelUp;


    [Header("Tratemento dos coletáveis")]
    [Tooltip("Define o tipo do item coletável")]
    public TipoItem tipo;

    [Tooltip("Define a velocidade de queda do item coletável")]
    public float velMoveCol;

    [Tooltip("Responsável pela física do item coletável")]
    [SerializeField] private Rigidbody2D rbd2Cos;

    private void Start()
    {
        Vector2 posCol = new Vector2(0, -1);
        posCol *= velMoveCol;
        rbd2Cos.velocity = posCol;
    }

    private void OnTriggerEnter2D(Collider2D outro)
    {
        if(outro.CompareTag("Player"))
        {
            Pontuacao.instance.Ponto(5);

            switch (tipo)
            {                
                case TipoItem.escudo:
                    SFX.instance.PlaySom(somEscudo);
                    outro.GetComponent<PlayerController>().RecebeCura(1);                   
                    Destroy(gameObject);
                    break;

                case TipoItem.lvUp:
                    SFX.instance.PlaySom(somLevelUp);
                    outro.GetComponent<PlayerController>().TiroLevel(1);
                    Destroy(gameObject);
                    break;

                default:
                    break;
            }
        }
    }

}
