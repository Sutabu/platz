﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personagem : MonoBehaviour
{
    [Header("Tratamento de Vida do Objeto")]

    [Tooltip("Define a vida máxima do objeto")]
    public float vidaMaxima;

    [Tooltip("Define a vida atual do objeto")]
    public float vidaAtual;

    [Tooltip("Define se está morto ou não")]
    public bool morto;


    [Header("Sons do Personagem")]

    [Tooltip("Som de quando toma dano")]
    public AudioClip somDano;

    public void RecebeDano(float dano)
    {
        SFX.instance.PlaySom(somDano);

        vidaAtual = Mathf.Max(vidaAtual - dano, 0);

        morto = vidaAtual <= 0;

        if (morto) Morte();
    }

    public void RecebeCura(float cura)
    {
        vidaAtual = Mathf.Min(vidaAtual  + cura, vidaMaxima);
    }

    public virtual void Morte()
    {
        Destroy(gameObject);
    }
}
